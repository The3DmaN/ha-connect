import QtQuick 2.12
import QtQuick.Controls 2.5
import QtWebEngine 1.10
import Qt.labs.settings 1.1

ApplicationWindow {
    id: window
    width: 360
    height: 720
    visible: true
    title: qsTr(" ")
    flags: {
        if (screen.width < 800) {
            Qt.FramelessWindowHint
        }
    }

    footer: ToolBar {
        id: toolBar
        contentHeight: toolButton.implicitHeight

        ToolButton {
            id: toolButton
            text: stackView.depth > 1 ? "\u25C0" : "\u2630"
            font.pixelSize: Qt.application.font.pixelSize * 1.6
            onClicked: {
                if (stackView.depth > 1) {
                    stackView.pop()
                } else {
                    drawer.open()
                }
            }
        }

        Label {
            text: stackView.currentItem.title
            anchors.centerIn: parent
        }
    }

    Drawer {
        id: drawer
        width: window.width * 0.66
        height: window.height

        Column {
            anchors.fill: parent

            Label {
                text: qsTr("HA Connect")
                width: parent.width
                horizontalAlignment: Text.AlignHCenter
                topPadding: 10
                bottomPadding: 10

            }
            ToolSeparator {
                orientation: Qt.Horizontal
                width: parent.width
            }
            ItemDelegate {
                text: qsTr("Settings")
                width: parent.width
                onClicked: {
                    stackView.push("AppSettings.ui.qml")
                    drawer.close()
                }
            }
            ItemDelegate {
                text: qsTr("About")
                width: parent.width
                onClicked: {
                    stackView.push("About.ui.qml")
                    drawer.close()
                }
            }
        }
    }

    StackView {
        id: stackView
        initialItem: "HAConnect.ui.qml"
        anchors.fill: parent
    }
    Settings {
        id: ipsettings
        property string ip: ""
        property string port: ""
    }
}
