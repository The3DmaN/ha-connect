import QtQuick 2.12
import QtQuick.Controls 2.5

Page {
    id: page
    title: qsTr("About")

    Column {
        id: column
        anchors.fill: parent
        anchors.margins: 20
        spacing: 5

        Image {
            id: image
            source: "files/images/ha-connect.svg"
            anchors.horizontalCenter: parent.horizontalCenter
            fillMode: Image.PreserveAspectFit
        }

        TextArea {
            id: descriptiontext
            text: "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\np, li { white-space: pre-wrap; }\n</style></head><body style=\" font-family:'Noto Sans'; font-size:11pt; font-weight:400; font-style:normal;\">\n<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">HA Connect is a simple web app for connecting to the web interface of a Home Assistant server. It is designed for Linux mobile but should also run on other platforms. This app is not an official Home Assistant app and is not maintained or supported by the Home Assistant team.</p></body></html>"
            horizontalAlignment: Text.AlignLeft
            wrapMode: Text.WordWrap
            anchors.horizontalCenter: parent.horizontalCenter
            textFormat: Text.RichText
            width: column.width
            readOnly: true
        }

        Label {
            id: linklabel
            text: "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\np, li { white-space: pre-wrap; }\n</style></head><body style=\" font-family:'Noto Sans'; font-size:11pt; font-weight:400; font-style:normal;\">\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><a name=\"https://the3dman.gitlab.io\"></a><a href=\"https://the3dman.gitlab.io\"><span style=\" text-decoration: underline; color:#0000ff;\">https://the3dman.gitlab.io</span></a> </p></body></html>"
            rightInset: 0
            leftInset: 0
            anchors.horizontalCenter: parent.horizontalCenter
            textFormat: Text.RichText
        }

        Label {
            id: copyrightlabel
            text: qsTr("Copyright © 2020 The3DmaN")
            anchors.horizontalCenter: parent.horizontalCenter
        }

    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
