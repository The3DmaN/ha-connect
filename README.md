# HA Connect

HA Connect is a simple web app for connecting to the web interface of an Home Assistant server. 

It is designed for Linux mobile but may also run on other platforms.


# Install:

**Manjaro/Arch:**

- Install from AUR (https://aur.archlinux.org/packages/ha-connect-git/)

**Mobian:**
- Download aarch64 deb from [here](https://gitlab.com/The3DmaN/the3dman.gitlab.io/-/tree/master/content/files/deb/haconnect) then in a terminal cd into download directory and install with `sudo apt-get install ./HA-Connect_XX_aarch64` Note: Replace "XX" with package version downloaded

# Known Issues

**Manjaro Plasma Mobile** - Menu button in header does not work

Workaround: Swipe to right to open

Bug submission: https://gitlab.manjaro.org/manjaro-arm/issues/pinephone/plasma-mobile/-/issues/36

# Screenshots:

![Screenshot](img/screenshot.png "HAC Screenshot")

# Important Note:

This app is not an official Home Assistant app and is not maintained or supported by the Home Assistant team.


